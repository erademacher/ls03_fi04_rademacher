import java.awt.BorderLayout;
import javax.swing.JColorChooser;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JToggleButton;
import javax.swing.JPasswordField;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.awt.SystemColor;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class GUI_rademacher extends JFrame {

	private JPanel contentPane;
	private JTextField txtBitteHierText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI_rademacher frame = new GUI_rademacher();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI_rademacher() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 665);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAufgabe1 = new JLabel("Aufgabe 1: Hintergrund \u00E4ndern");
		lblAufgabe1.setBounds(10, 101, 274, 27);
		contentPane.add(lblAufgabe1);
		
		JButton btnRed = new JButton("Red");
		btnRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonRed_clicked();
			}
		});
		btnRed.setBounds(10, 139, 130, 23);
		contentPane.add(btnRed);
		
		JButton btnBlue = new JButton("Blue");
		btnBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonBlue_clicked();
			}
		});
		btnBlue.setBounds(154, 139, 130, 23);
		contentPane.add(btnBlue);
		
		JButton btnGreen = new JButton("Green");
		btnGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonGreen_clicked();
			}
		});
		btnGreen.setBounds(300, 139, 130, 23);
		contentPane.add(btnGreen);
		
		JButton btnYellow = new JButton("Yellow");
		btnYellow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonYellow_clicked();
			}
		});
		btnYellow.setBounds(10, 173, 130, 23);
		contentPane.add(btnYellow);
		
		JButton btnStandard = new JButton("Standardfarbe");
		btnStandard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonStandard_clicked();
			}
		});
		btnStandard.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnStandard.setBounds(154, 173, 130, 23);
		contentPane.add(btnStandard);
		
		JButton btnFarbenWaehlen = new JButton("Farben w\u00E4hlen");
		btnFarbenWaehlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonFarbenWaehlen_clicked();
			}
		});
		btnFarbenWaehlen.addContainerListener(new ContainerAdapter() {
			
		});
		btnFarbenWaehlen.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnFarbenWaehlen.setBounds(300, 173, 130, 23);
		contentPane.add(btnFarbenWaehlen);
		
		JLabel lblAufgabe2 = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabe2.setBounds(10, 207, 274, 27);
		contentPane.add(lblAufgabe2);
		
		txtBitteHierText = new JTextField();
		txtBitteHierText.setText("Bitte hier Text eingeben");
		txtBitteHierText.setBounds(10, 279, 420, 20);
		contentPane.add(txtBitteHierText);
		txtBitteHierText.setColumns(10);
		
		
		JLabel lblAufgabe3 = new JLabel("Aufgabe3: Schriftfarbe \u00E4ndern");
		lblAufgabe3.setBounds(10, 344, 244, 14);
		contentPane.add(lblAufgabe3);
				
		JLabel lblAufgabe4 = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblAufgabe4.setBounds(10, 403, 420, 14);
		contentPane.add(lblAufgabe4);
		
		JLabel lblAufgabe5 = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabe5.setBounds(10, 462, 420, 14);
		contentPane.add(lblAufgabe5);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			System.exit(1);
			}
		});
		btnExit.setBounds(10, 546, 420, 70);
		contentPane.add(btnExit);
		
		JLabel lblLeer = new JLabel("");
		lblLeer.setHorizontalAlignment(SwingConstants.CENTER);
		lblLeer.setBounds(10, 48, 414, 20);
		contentPane.add(lblLeer);
		lblLeer.setText(txtBitteHierText.getText());
		
		
		
		JLabel lblExit = new JLabel("Aufgabe 6: Programm beenden");
		lblExit.setBounds(10, 521, 420, 14);
		contentPane.add(lblExit);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			lblLeer.setFont(new Font("Arial", Font.PLAIN, 12));


			}
		
		});
		
		btnArial.setBounds(10, 245, 130, 23);
		contentPane.add(btnArial);
		
		JButton btnComicSansMS = new JButton("Comic Sans MS");
		btnComicSansMS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			lblLeer.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));

			}
		});
		btnComicSansMS.setBounds(154, 245, 130, 23);
		contentPane.add(btnComicSansMS);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			lblLeer.setFont(new Font("Courier New", Font.PLAIN, 12));
			}
		});
		btnCourierNew.setBounds(300, 245, 130, 23);
		contentPane.add(btnCourierNew);
		
		JButton btnLabelSchreiben = new JButton("Ins Label schreiben");
		btnLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			lblLeer.setText(txtBitteHierText.getText());
				
			}
		});
		btnLabelSchreiben.setBounds(10, 310, 200, 23);
		contentPane.add(btnLabelSchreiben);
		
		JButton btnLabelLoeschen = new JButton("Text im Label l\u00F6schen");
		btnLabelLoeschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			lblLeer.setText("");

			

			}
		});
		btnLabelLoeschen.setBounds(230, 310, 200, 23);
		contentPane.add(btnLabelLoeschen);
		
		JButton btnSchriftRot = new JButton("Rot");
		btnSchriftRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			lblLeer.setForeground(Color.RED);
			}
		});
		btnSchriftRot.setBounds(10, 369, 89, 23);
		contentPane.add(btnSchriftRot);
		
		JButton btnSchriftBlau = new JButton("Blau");
		btnSchriftBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			lblLeer.setForeground(Color.BLUE);
			}
		});
		btnSchriftBlau.setBounds(179, 369, 89, 23);
		contentPane.add(btnSchriftBlau);
		
		JButton btnSchriftSchwarz = new JButton("Schwarz");
		btnSchriftSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			lblLeer.setForeground(Color.BLACK);

			}
		});
		btnSchriftSchwarz.setBounds(341, 369, 89, 23);
		contentPane.add(btnSchriftSchwarz);
		
		JButton btnSchriftGroesser = new JButton("+");
		btnSchriftGroesser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			int groesse = lblLeer.getFont().getSize();
			lblLeer.setFont(new Font("Arial", Font.PLAIN, groesse + 1));
			}
		});
		btnSchriftGroesser.setBounds(10, 428, 200, 23);
		contentPane.add(btnSchriftGroesser);
		
		JButton btnSchriftKleiner = new JButton("-");
		btnSchriftKleiner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			int groesse = lblLeer.getFont().getSize();
			lblLeer.setFont(new Font("Arial", Font.PLAIN, groesse - 1));
			}
		});
		btnSchriftKleiner.setBounds(230, 428, 200, 23);
		contentPane.add(btnSchriftKleiner);
		
		JButton btnSchriftLinks = new JButton("Linksb\u00FCndig");
		btnSchriftLinks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			lblLeer.setHorizontalAlignment(SwingConstants.LEFT);

			}
		});
		btnSchriftLinks.setBounds(10, 487, 110, 23);
		contentPane.add(btnSchriftLinks);
		
		JButton btnSchriftZentriert = new JButton("Zentriert");
		btnSchriftZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			lblLeer.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnSchriftZentriert.setBounds(179, 487, 89, 23);
		contentPane.add(btnSchriftZentriert);
		
		JButton btnSchriftRechts = new JButton("Rechtsb\u00FCndig");
		btnSchriftRechts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			lblLeer.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnSchriftRechts.setBounds(320, 487, 110, 23);
		contentPane.add(btnSchriftRechts);
		
	}

	public void buttonFarbenWaehlen_clicked() {
		Color ausgewaehlteFarbe = JColorChooser.showDialog(null,
				"Farbauswahl",null);
		this.contentPane.setBackground(ausgewaehlteFarbe);
		
	}

	public void buttonStandard_clicked() {
		this.contentPane.setBackground(Color.WHITE);
		
	}

	public void buttonYellow_clicked() {
		this.contentPane.setBackground(Color.YELLOW);
		
	}

	public void buttonGreen_clicked() {
		this.contentPane.setBackground(Color.GREEN);
		
	}

	public void buttonBlue_clicked() {
		this.contentPane.setBackground(Color.BLUE);
		}

	public void buttonRed_clicked() {
		this.contentPane.setBackground(Color.RED);
		}
	
	
}


